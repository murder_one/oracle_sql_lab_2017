CREATE TABLE distributor (
    id INT NOT NULL PRIMARY KEY,
    name varchar2(30) NOT NULL
);

BEGIN
INSERT INTO distributor (id, name) VALUES (1, 'Corp1');
INSERT INTO distributor (id, name) VALUES (2, 'Corp2');
INSERT INTO distributor (id, name) VALUES (3, 'Corp3');
INSERT INTO distributor (id, name) VALUES (4, 'Corp4');
INSERT INTO distributor (id, name) VALUES (5, 'Corp5');
INSERT INTO distributor (id, name) VALUES (6, 'Corp6');
INSERT INTO distributor (id, name) VALUES (7, 'Corp7');
INSERT INTO distributor (id, name) VALUES (8, 'Corp8');
END;

CREATE TABLE product (
    id INT NOT NULL PRIMARY KEY,
    id_dist INT NOT NULL,  
    name varchar2(30) NOT NULL,
    delivery_date date NOT NULL,
    term date NOT NULL,
    price INT NOT NULL,
    count INT NOT NULL,
    foreign key (id_dist) references distributor(id)
);

BEGIN
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (1, 1, 'Milk', TO_DATE('07-MAR-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('10-MAR-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), 1100, 150);
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (2, 2, 'Sugar', TO_DATE('02-APR-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('02-MAY-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), 200, 70);
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (3, 3, 'Beer', TO_DATE('05-APR-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('05-MAY-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), 50, 100);
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (4, 4, 'Vodka', TO_DATE('02-FEB-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('02-FEB-2019 12:30:00','DD-MON-YYYY HH:MI:SS'), 60, 200);
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (5, 5, 'Jack Daniels', TO_DATE('01-JAN-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('01-JAN-2019 12:30:00','DD-MON-YYYY HH:MI:SS'), 1000, 200);
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (6, 6, 'Jim Bean', TO_DATE('09-JAN-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('09-JAN-2019 12:30:00','DD-MON-YYYY HH:MI:SS'), 900, 300);
INSERT INTO product (id, id_dist, name, delivery_date, term, price, count)
VALUES (7, 7, 'Scotch', TO_DATE('03-APR-2017 12:30:00','DD-MON-YYYY HH:MI:SS'), TO_DATE('03-APR-2019 12:30:00','DD-MON-YYYY HH:MI:SS'), 950, 400);
END;

/*TASKS FOR LAB 2*/
--1
--term_date - критерий для скоропортящигося товара
CREATE OR REPLACE FUNCTION add_one(term_date IN DATE) RETURN NUMBER IS cnumber NUMBER;
	CURSOR c1 IS
	SELECT * FROM product WHERE id = id_pr AND (TO_DATE(delivery_date, 'DD')-TO_DATE(term, 'DD'))=>TO_DATE(term_date, 'DD');
DECLARE
	i NUMBER := 1;
    BEGIN
    LOOP
	OPEN c1;
	FETCH c1
		INTO cnumber;
		IF c1%notfound
		THEN
--долгосрочный
		UPDATE product SET price=price-price*0.2 WHERE id=i
			AND (price*0.2)=<50
			AND TO_DATE('01', 'DD')<(SELECT (TO_DATE(term, 'DD-MON-YYYY HH:MI:SS')-TO_DATE(SYSDATE, 'DD-MON-YYYY HH:MI:SS')) AS term2 
				FROM product);
		UPDATE product SET price=price-50 WHERE id=i
			AND (price*0.2)>50
			AND TO_DATE('01', 'DD')<(SELECT (TO_DATE(term, 'DD-MON-YYYY HH:MI:SS')-TO_DATE(SYSDATE, 'DD-MON-YYYY HH:MI:SS')) AS term2 
				FROM product);
		ELSE
--скоропортящийся
		UPDATE product SET price=price-price*0.2 WHERE id=i
			AND (price*0.2)=<50
			AND TO_DATE('03', 'HH')<(SELECT (TO_DATE(term, 'DD-MON-YYYY HH:MI:SS')-TO_DATE(SYSDATE, 'DD-MON-YYYY HH:MI:SS')) AS term2 
				FROM product);
		UPDATE product SET price=price-50 WHERE id=i
			AND (price*0.2)>50
			AND TO_DATE('03', 'HH')<(SELECT (TO_DATE(term, 'DD-MON-YYYY HH:MI:SS')-TO_DATE(SYSDATE, 'DD-MON-YYYY HH:MI:SS')) AS term2 
				FROM product);
		END IF;
		CLOSE c1;
	RETURN cnumber;
	EXCEPTION
		WHEN OTHERS THEN raise_application_error(-20001,'An error was encountered - ' || SQLCODE || ' -ERROR- ' || SQLERRM);
	EXIT WHEN i=7;
    END LOOP;
END;

--2
--таблица бекапа цен
CREATE TABLE prices(
    id INT NOT NULL PRIMARY KEY,
    price INT NOT NULL
);

--создаем бекап цен
CREATE OR REPLACE PROCEDURE old_prices(x INT, y INT)
    AS
	DECLARE
		i NUMBER := x;
    BEGIN
	LOOP
		INSERT INTO prices (id, price)
		SELECT id, price FROM product WHERE id=i;
		EXIT WHEN i=y;
	END LOOP;
END old_prices;

-- основное задание
CREATE OR REPLACE PROCEDURE add_two(x INT, y INT) IS
DECLARE
	i NUMBER := x;
BEGIN
    ALTER TABLE product ADD status varchar2(30);
	LOOP
		UPDATE product SET status='short-term' WHERE id=i
			AND (TO_DATE(delivery_date, 'DD')-TO_DATE(term, 'DD'))=>TO_DATE('03', 'DD');
		UPDATE product SET status='long-term' WHERE id=i
			AND (TO_DATE(delivery_date, 'DD')-TO_DATE(term, 'DD'))<TO_DATE('03', 'DD');
		EXIT WHEN i=y;
	END LOOP;
	SELECT product.status, product.id, product.name, product.count, prices.price, product.price 
	FROM prices INNER JOIN product on prices.id=product.id;
END;

--запуск всего, что нужно
BEGIN
	old_prices (1, 7); --начианя с какого и до какого ID бекапить 
	add_one (3); --3 условие для скоропортящегося товара
	add_two (1, 7); --начианя с какого и до какого IDвыводить
END;

/*TASKS FOR LAB 3*/
--1
--добавление информации об остатке
ALTER TABLE product ADD rest INT;

DECLARE
    i NUMBER := 1;
BEGIN
	LOOP
		UPDATE product SET rest = count - 25 WHERE id=i;
		i := i+1;
		EXIT WHEN i = 7;
	END LOOP;
END;

CREATE OR REPLACE TRIGGER add_three
AFTER INSERT ON product
FOR EACH ROW
BEGIN
    DELETE FROM product WHERE count>0 OR price>0 OR delivery_date>SYSDATE OR rest<count OR rest>0;
UPDATE product SET delivery_date=SYSDATE WHERE delivery_date IS NULL;
END;


--2
--спец таблица
CREATE TABLE spec_product (
    id INT NOT NULL PRIMARY KEY,
    id_dist INT NOT NULL,  
    name varchar2(30) NOT NULL,
    delivery_date date NOT NULL,
    term date NOT NULL,
    price INT NOT NULL,
    count INT NOT NULL,
	status varchar2(30),
	rest INT,	
    foreign key (id_dist) references distributor(id)
);

CREATE OR REPLACE TRIGGER add_four
AFTER INSERT ON product
FOR EACH ROW
DECLARE
    i NUMBER := 1;
BEGIN
	LOOP
		INSERT INTO spec_product (id, id_dist, name, delivery_date, term, price, count, status, rest) SELECT * FROM product WHERE id=i;
		i := i+1;
		EXIT WHEN i = 7;
	END LOOP;
END;
