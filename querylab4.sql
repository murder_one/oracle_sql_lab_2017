--https://habrahabr.ru/post/256655/
--1 Представление VIEW
CREATE OR REPLACE VIEW clone AS SELECT card.id, card.id_doctor, card.id_patient, card.id_medicines, medicines.name, card.treat_begin, card.treat_end, card.diagnosis, card.count
FROM medicines INNER JOIN card ON medicines.id = card.id_medicines

--2 Триггер instead of на представление
CREATE OR REPLACE TRIGGER four_lab
INSTEAD OF INSERT ON clone
BEGIN
    INSERT INTO medicines (id, name, price) VALUES (666, 'Test_medicine', 666);
END;
   
--3 Процедуры/функции/триггеры, включающие в себя обработку всех видов исключительных ситуаций
CREATE OR REPLACE TRIGGER four_lab_2
    INSTEAD OF INSERT ON clone
    DECLARE
		err_med EXCEPTION;
		err_pat EXCEPTION;
                err_count EXCEPTION;
                err_doc EXCEPTION;
BEGIN
    INSERT INTO medicines (id, name, price) VALUES (777, 'Test_medicine_2', 7777);
	IF :NEW.id_medicines NOT BETWEEN 1 AND 10 THEN RAISE err_med ;  END IF;
	IF :NEW.id_patient NOT BETWEEN 1 AND 900 THEN RAISE err_pat;  END IF;
	IF :NEW.id_doctor NOT BETWEEN 1 AND 10 THEN RAISE err_doc ;  END IF;
	IF :NEW.count NOT BETWEEN 25 AND 900 THEN RAISE err_count;  END IF;
    EXCEPTION
    	WHEN err_med THEN RAISE_APPLICATION_ERROR(-20005,'Id medicine must be between 1 and 10');
    	WHEN err_pat THEN RAISE_APPLICATION_ERROR(-20006,'Id patient must be between 1 and 900');
        WHEN err_doc THEN RAISE_APPLICATION_ERROR(-20005,'Id doctor must be between 1 and 10');
        WHEN err_count THEN RAISE_APPLICATION_ERROR(-20006,'Count must be between 25 and 900');
END;
