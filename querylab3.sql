--Написать предваряющий триггер на изменение вставляемых/удаляемых/обновляемых данных в этой же таблице.
--Если цена добавляемого препарата менее 5000 гривен - приравнять ее к 50000 гривнам.

CREATE OR REPLACE TRIGGER trig1
BEFORE INSERT ON medicines
FOR EACH ROW
BEGIN
  IF (:NEW.price < 5000) THEN
    :NEW.price := 100;
  END IF;
END;


--Написать завершающий триггер, приводящий к изменениям в другой таблице.
--Добавить новый метод лечения после того, как будет добавлена новая специализация.
--Так же, если специализация будет убрана - повысить на 1000 грн цену первого препарата

CREATE OR REPLACE TRIGGER trig2
AFTER INSERT OR DELETE ON specialization
FOR EACH ROW
BEGIN 
  CASE 
    WHEN INSERTING THEN INSERT INTO process (id, name) VALUES (88, 'Megainjection');
    WHEN DELETING THEN UPDATE medicines SET price=price+1000 WHERE id=1;
  END CASE;
END;


--Написать триггер с использованием автономной транзакции.
--Добавить доктора после прихода пациента.

CREATE OR REPLACE TRIGGER trig3
BEFORE INSERT ON patient
FOR EACH ROW
DECLARE 
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
  INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name)
    VALUES(88, 3, 'f322309k', 5, 'Jack', 'Jackerman');
  COMMIT;
END;


--Произвести действия в таблицах, приводящие к запуску триггеров.

1) INSERT INTO medicines (id, name, price) VALUES (88, 'Tripplemorphin', 30000);
   SELECT * FROM medicines
   DROP TRIGGER trig1

2) DELETE FROM specialization where id = 8;
   INSERT INTO specialization (id, name) VALUES (88, 'Ultra_injector');
   SELECT * FROM process
   SELECT * FROM medicines
   DROP TRIGGER trig2
   
3) INSERT INTO patient (id, first_name, last_name, birthday) VALUES (99, 'Elizaveta', 'Elizevich', TO_DATE('08-MAR-1999','DD-MON-YYYY'));
   SELECT * FROM doctor
   DROP TRIGGER trig3
   