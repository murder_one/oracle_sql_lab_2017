CREATE TABLE specialization (
  id int NOT NULL PRIMARY KEY,
  name varchar2(30) NOT NULL
);

BEGIN
INSERT INTO specialization (id, name) VALUES (1, 'Pediatrician');
INSERT INTO specialization (id, name) VALUES (2, 'Traumatologist');
INSERT INTO specialization (id, name) VALUES (3, 'Surgeon');
INSERT INTO specialization (id, name) VALUES (4, 'Drug_injector');
INSERT INTO specialization (id, name) VALUES (5, 'Enema_injector');
INSERT INTO specialization (id, name) VALUES (6, 'Multi_injector');
INSERT INTO specialization (id, name) VALUES (7, 'Pathologist');
INSERT INTO specialization (id, name) VALUES (8, 'Mega_injector');
END;

CREATE TABLE process (
  id int NOT NULL,
  name varchar2(30) NOT NULL,
  PRIMARY KEY (id)
);

BEGIN
INSERT INTO process (id, name) VALUES (1, 'Procedure');
INSERT INTO process (id, name) VALUES (2, 'Medicine');
INSERT INTO process (id, name) VALUES (3, 'Enema');
INSERT INTO process (id, name) VALUES (4, 'Injections');
INSERT INTO process (id, name) VALUES (5, 'Operation');
INSERT INTO process (id, name) VALUES (6, 'Oral_method');
INSERT INTO process (id, name) VALUES (7, 'Multiinjection');
END;


CREATE TABLE medicines (
    id INT NOT NULL PRIMARY KEY,
    name varchar2(40) NOT NULL,
    price int NOT NULL
);

BEGIN
INSERT INTO medicines (id, name, price) VALUES (1, 'Tropikomid', 7000);
INSERT INTO medicines (id, name, price) VALUES (2, 'Meskalin', 5000);
INSERT INTO medicines (id, name, price) VALUES (3, 'Psycislocibin', 6000);
INSERT INTO medicines (id, name, price) VALUES (4, 'Lirika', 3000);
INSERT INTO medicines (id, name, price) VALUES (5, 'Tramadol', 5000);
INSERT INTO medicines (id, name, price) VALUES (6, 'Diteltriptamin', 4000);
INSERT INTO medicines (id, name, price) VALUES (7, 'Dietilamid', 5000);
INSERT INTO medicines (id, name, price) VALUES (8, 'Empatogenu', 10000);
INSERT INTO medicines (id, name, price) VALUES (9, 'Zolpidem', 3000);
INSERT INTO medicines (id, name, price) VALUES (10, 'Desomorphine', 9000);
INSERT INTO medicines (id, name, price) VALUES (11, 'Etorphine', 4000);
INSERT INTO medicines (id, name, price) VALUES (12, 'Barbiturate', 1000);
END;



CREATE TABLE patient (
    id INT NOT NULL PRIMARY KEY,
    first_name varchar2(30) NOT NULL,
    last_name varchar2(30) NOT NULL,
    birthday date NOT NULL
);

BEGIN
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (1, 'Eduard', 'Edurman', TO_DATE('07-MAR-2000','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (2, 'Mikhail', 'Mikhalberg', TO_DATE('13-NOV-1988','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (3, 'Igor', 'Igorevich', TO_DATE('14-SEP-1977','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (4, 'Vasily', 'Vasilberg', TO_DATE('12-MAR-2001','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (5, 'Alexandr', 'Alexandrevskiy', TO_DATE('24-MAY-1990','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (6, 'Vladimir', 'Vladirovich', TO_DATE('15-JUL-1989','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (7, 'Ivan', 'Ivanberg', TO_DATE('20-JUN-1978','DD-MON-YYYY'));
INSERT INTO patient (id, first_name, last_name, birthday) VALUES (8, 'Arseniy', 'Arsenman', TO_DATE('07-AUG-1977','DD-MON-YYYY'));
END;


CREATE TABLE doctor (
  id int NOT NULL PRIMARY KEY,
  id_specialization int NOT NULL,
  password varchar2(10) NOT NULL,
  id_process int NOT NULL,
  first_name varchar2(20) NOT NULL,
  last_name varchar2(20) NOT NULL,
  foreign key (id_specialization) references specialization(id),
  foreign key (id_process) references process(id)
);

BEGIN
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (1, 1, '122d1e21', 1, 'Egor', 'Egorman');
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (2, 2, 'f3h35h43', 2, 'Alexey', 'Alexevich');
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (3, 3, '332gtfo5', 3, 'Petr', 'Peterman');
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (4, 4, '12acab22', 4, 'Dmitry', 'Dmitterman');
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (5, 5, 'wtf413y3', 4, 'Vladimir', 'Vladirovsky');
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (6, 6, '54lol223', 4, 'Pavel', 'Pavelberg');
INSERT INTO doctor (id, id_specialization, password, id_process, first_name, last_name) VALUES (7, 7, 'pfed228z', 4, 'David', 'Davidovich');
END;

CREATE TABLE card (
  id INT NOT NULL PRIMARY KEY,
  id_doctor int NOT NULL,
  id_patient int NOT NULL,
	id_medicines int NOT NULL,
	treat_begin date NOT NULL,
	treat_end date NOT NULL,
	diagnosis varchar2(150),
  count int NOT NULL,
	foreign key (id_medicines) references medicines(id),
	foreign key (id_doctor) references doctor(id),
	foreign key (id_patient) references patient(id)
);

BEGIN
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (1, 1, 1, 10, TO_DATE('09-JAN-2017','DD-MON-YYYY'), TO_DATE('09-FEB-2017','DD-MON-YYYY'), 'Schizophrenia', 23); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (2, 2, 2, 5, TO_DATE('04-FEB-2017','DD-MON-YYYY'), TO_DATE('22-FEB-2017','DD-MON-YYYY'), 'Schizophrenia', 11); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (3, 3, 3, 10, TO_DATE('08-AUG-2017','DD-MON-YYYY'), TO_DATE('08-SEP-2017','DD-MON-YYYY'), 'Diarea', 6); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (4, 3, 4, 3, TO_DATE('12-NOV-2017','DD-MON-YYYY'), TO_DATE('12-DEC-2017','DD-MON-YYYY'), 'Flu', 1); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (5, 2, 5, 7, TO_DATE('21-JUN-2017','DD-MON-YYYY'), TO_DATE('21-OCT-2017','DD-MON-YYYY'), 'Schizophrenia', 9); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (6, 6, 6, 8, TO_DATE('13-MAR-2017','DD-MON-YYYY'), TO_DATE('13-SEP-2017','DD-MON-YYYY'), 'Dysentery', 2); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (7, 7, 7, 10, TO_DATE('16-JUN-2017','DD-MON-YYYY'), TO_DATE('16-APR-2018','DD-MON-YYYY'), 'Deviation', 4); 
insert into card (id, id_doctor, id_patient, id_medicines, treat_begin, treat_end, diagnosis, count) values (8, 2, 8, 12, TO_DATE('01-JAN-2017','DD-MON-YYYY'), TO_DATE('01-MAR-2017','DD-MON-YYYY'), 'Psycho', 5); 
END;

CREATE TABLE analyzes (
    id INT NOT NULL PRIMARY KEY,
	name varchar2(30) NOT NULL,
    results varchar2(30) NOT NULL,
    id_card INT NOT NULL,
    price int NOT NULL,
    foreign key (id_card) references card(id)
);

BEGIN
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (1, 'Alcoholism', 'No', 1, 200);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (2, 'Schizophrenia', 'Yes', 2, 300);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (3, 'Diarea', 'Yes', 3, 400);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (4, 'Flu', 'Yes', 4, 100);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (5, 'HIV', 'No', 5, 900);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (6, 'Dysentery', 'Yes', 6, 100);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (7, 'Deviation', 'Yes', 7, 300);
INSERT INTO analyzes (id, name, results, id_card, price) VALUES (8, 'Psycho', 'Yes', 8, 100);
END;


BEGIN
DROP TABLE analyzes;
DROP TABLE card;
DROP TABLE doctor;
DROP TABLE patient;
DROP TABLE medicines;
DROP TABLE process;
DROP TABLE specialization;
END;