/*1 Вывести данные пациента (ФИ), и данные начала и окончания лечения*/
SELECT patient.first_name, patient.last_name, card.*
FROM patient INNER JOIN card ON patient.id = card.id_patient

/*2 Вывести информацию о докторах, которые лечат больных клизьмой*/
SELECT doctor.*, process.*
FROM doctor INNER JOIN process ON doctor.id_process = process.id
WHERE process.name = 'Enema'

/*3 Вывести информацию о специальностях, а так же, сколько медиков на каждой из них*/
SELECT specialization.*, COUNT(doctor.id) 
FROM doctor INNER JOIN specialization ON doctor.id_specialization = specialization.id
GROUP BY specialization.id, specialization.name

/*4 Выбрать пациентов с диагнозом "Шизофрения"*/
SELECT patient.*, card.diagnosis
FROM patient INNER JOIN card ON patient.id = card.id_patient
WHERE card.diagnosis = 'Schizophrenia';

/*5 Вывести информацию о докторах и ФИ пациентов, которых он лечит*/
SELECT doctor.*, patient.first_name, patient.last_name
FROM doctor INNER JOIN card ON doctor.id = card.id_doctor INNER JOIN patient ON card.id_patient = patient.id

/*6 Выбрать доктора, который лечит пациентов Трамадолом*/
SELECT doctor.*, medicines.name
FROM doctor INNER JOIN card ON doctor.id = card.id_doctor INNER JOIN medicines ON card.id_medicines = medicines.id
WHERE medicines.name = 'Tramadol'

/*7 Вывести, сколько пациентов пользуются тем или иным процессом лечения*/
SELECT process.name, COUNT(patient.id)
FROM patient INNER JOIN card ON patient.id=card.id_patient INNER JOIN doctor ON card.id_doctor = doctor.id INNER JOIN process ON doctor.id_process = process.id
GROUP BY process.name

/*8 Вывести, сколько пациентов пользуются тем или иным препаратом*/
SELECT medicines.*, COUNT(patient.id)
FROM patient INNER JOIN card ON patient.id = card.id_patient INNER JOIN medicines ON card.id_medicines = medicines.id
GROUP BY medicines.id, medicines.name

/*9 Вывести информацию о докторах и их специальностях*/
SELECT doctor.*, specialization.name 
FROM doctor INNER JOIN specialization ON doctor.id_specialization = specialization.id

/*10 Вывести данные об анализах пациентов и их картах*/
SELECT analyzes.*, card.*
FROM card INNER JOIN analyzes ON card.id=analyzes.id_card

/*11 Вывести фамилию пациентов и их анализы*/
SELECT patient.last_name, analyzes.*
FROM patient INNER JOIN card ON patient.id=card.id_patient INNER JOIN analyzes ON card.id=analyzes.id_card

/*12 Вывести пациентов и процесс их лечения*/
SELECT patient.*, process.name
FROM patient INNER JOIN card ON patient.id=card.id_patient INNER JOIN doctor ON card.id_doctor = doctor.id INNER JOIN process ON doctor.id_process = process.id

/*13 Вывести пациентов и препараты, которые они принимают*/
SELECT patient.*, medicines.name
FROM patient INNER JOIN card ON patient.id = card.id_patient INNER JOIN medicines ON card.id_medicines = medicines.id

/*14 Вывести информацию о докторах и их специальностях с помощью UNION*/
SELECT specialization.id, name
FROM specialization
UNION ALL
SELECT doctor.id_specialization , last_name
FROM doctor

/*15 Выбрать пациентов с диагнозом "Шизофрения"*/
SELECT patient.id, last_name
FROM patient
UNION ALL
SELECT card.id_patient, diagnosis
FROM card
WHERE card.diagnosis = 'Schizophrenia';

/*16 Вывести информацию о докторах, которые лечат больных препаратами*/
SELECT *
FROM doctor
WHERE doctor.id IN (SELECT id FROM process WHERE name = 'Medicine')

/*17 Выбрать доктора, который лечит пациентов Дезоморфином*/
SELECT *
FROM doctor
WHERE doctor.id IN (SELECT id FROM medicines WHERE name = 'Desomorphine')

/*18 Выбрать пациентов с диагнозом "Девиация"*/
SELECT *
FROM patient
WHERE patient.id IN (SELECT id FROM card WHERE diagnosis = 'Deviation')

/*19 Вывести пациентов, которых лечат процедурами*/
SELECT patient.*, process.name
FROM patient INNER JOIN card ON patient.id=card.id_patient INNER JOIN doctor ON card.id_doctor = doctor.id INNER JOIN process ON doctor.id_process = process.id
WHERE patient.id IN (SELECT id FROM process WHERE name = 'Procedure')

/*20 Вывести фамилию пациентов, которые сдавали анализы на алкоголизм и их результаты*/
SELECT patient.last_name, analyzes.*
FROM patient INNER JOIN card ON patient.id=card.id_patient INNER JOIN analyzes ON card.id=analyzes.id_card
WHERE patient.id IN (SELECT id from analyzes WHERE name = 'Alcoholism')