/*1 Если у препарата №5 цена больше 50000грн - приравнять ее
к 50000грн. В противном случае, приравнять к 50000грн цену соседнего
препарата*/

     DECLARE
        a NUMBER;
     BEGIN
        SELECT price INTO a FROM medicines WHERE id=5;
        IF a<5000 THEN
           UPDATE medicines SET price=5000 WHERE id=5; 
        ELSE
           UPDATE medicines SET price=5000 WHERE id=6;
        END IF;
     END;

/*2 Если в карте №3 цена анализов 100грн - приравнять ее
к 100грн.*/

     DECLARE
        a NUMBER;
     BEGIN
        SELECT price INTO a
        FROM analyzes WHERE id_card=3;
        IF a<100 THEN
           UPDATE analyzes SET price=100 WHERE id_card=3;
        END IF;
     END;

/*3 Сменить пароль на 8888 доктору №2, елси его ID специальности
= 4. В противном случае сменить пароль на 1111*/

    DECLARE
        a NUMBER;
    BEGIN
        SELECT id_specialization INTO a 
        FROM doctor WHERE id=2;
        IF a=4 THEN
           UPDATE doctor SET password=8888 WHERE id=2;
        ELSE
           UPDATE doctor SET password=1111 WHERE id=2;
        END IF;
     END;

/*4 Добавлять в аптечное хранилище дозы дезоморфина разной
цены, пока количество видов препаратов не станет = 25*/

    DECLARE
        i NUMBER := 21;
        c NUMBER := 6000;
    BEGIN
        LOOP
           INSERT INTO medicines (id, name, price) VALUES(i, 'Desomorphine', c);
           i := i+1;
           c := c+200;
           EXIT WHEN i=26;
        END LOOP;
    END;

/*5 Каждому пациенту добавить по дозе*/

    DECLARE
        i NUMBER := 1;
    BEGIN
        LOOP
           UPDATE card SET count=count+1 WHERE id=i;
           i := i+1;
           EXIT WHEN i=8;
        END LOOP;
    END;

/*6 Увеличить дату конца на месяц пациенту №5, если у 
него диагноз "шизофрения". В противном случае - 
продержать на три месяца дольше.*/

    DECLARE
        a varchar2(30);
    BEGIN
        SELECT diagnosis INTO a FROM card WHERE id_patient=5;
        IF a='Schizophreina' THEN
            UPDATE card SET treat_end=ADD_MONTHS(treat_end, 1) 
            WHERE id_patient=5;
        ELSE
            UPDATE card SET treat_end=ADD_MONTHS(treat_end, 3)
            WHERE id_patient=5;
        END IF;
    END;

/*7 Если пациент №4 использует домашний дешевый препарат меньше, 
чем за 2000грн - добавить ему еще 4 месяца, в противном случае, убавить месяц*/

    DECLARE
        a NUMBER;
    BEGIN
        SELECT medicines.price INTO a
        FROM card INNER JOIN medicines ON card.id_medicines=medicines.id
        WHERE card.id_patient=4;
        IF a>200 THEN
           UPDATE card SET treat_end=ADD_MONTHS(treat_end, 4)
           WHERE id_patient=4;
        ELSE
           UPDATE card SET treat_end=ADD_MONTHS(treat_end, -1)
           WHERE id_patient=4;
        END IF;
    END;

/*8 Если пациента №1 лечит доктор Арсений - убавить месяц*/

    DECLARE
        a varchar2(30);
    BEGIN
        SELECT doctor.first_name INTO a
        FROM doctor INNER JOIN card ON doctor.id=card.id_doctor
        WHERE card.id_patient=1;
        IF a='Arseniy' THEN
           UPDATE card SET treat_end=ADD_MONTHS(treat_end, -1)
           WHERE id_patient=1;
        END IF;
    END;

/*9 Если пациента №7 лечат клизмой - добавить 5 месяцев
и добавить 50 процедур*/

    DECLARE
        a varchar2(30);
    BEGIN
        SELECT process.name INTO a
        FROM doctor INNER JOIN card ON doctor.id=card.id_doctor INNER JOIN process on doctor.id_process=process.id
        WHERE card.id_patient=7;
        IF a='Enema' THEN
            BEGIN
                UPDATE card SET treat_end=ADD_MONTHS(treat_end, 5) WHERE id_patient=7;
                UPDATE card SET count=count+50 WHERE id_patient=7;
            END;
        END IF;
    END;

/*10 Если пациенту №5 прописано менее 20-ти инъекций - добавить
5 месяцев и 50 инъекций, в противном случае добавить месяц и еще
20 инъекций его соседу*/

    DECLARE
        a INT;
    BEGIN
        SELECT card.count INTO a
        FROM card INNER JOIN doctor ON card.id_doctor=doctor.id
        INNER JOIN process ON doctor.id_process=process.id
        WHERE card.id_patient=6 AND process.name='Injections';
        IF a<20 THEN
            BEGIN
                UPDATE card SET treat_end=ADD_MONTHS(treat_end, 5) WHERE id_patient=6;
                UPDATE card SET count=count+50 WHERE id_patient=6;
            END;
        ELSE
            BEGIN
                UPDATE card SET treat_end=ADD_MONTHS(treat_end, 1) WHERE id_patient=6;
                UPDATE card SET count=count+20 WHERE id_patient=7;
            END;
        END IF;
    END;

/*С помощью курсора поменять вывести данные*/

DECLARE
        x medicines.name%TYPE;
    CURSOR get_info(id_1 medicines.id%TYPE) IS
        SELECT name FROM medicines
        WHERE id=id_1;
BEGIN
        DBMS_OUTPUT.enable;
        OPEN get_info(10);
 LOOP
        FETCH get_info INTO x;
        EXIT WHEN get_info%NOTFOUND;
        
    DBMS_OUTPUT.put_line(TO_CHAR(x));
 END LOOP;
    CLOSE get_info;
END;

/*Процедура на изменение цены*/
CREATE OR REPLACE PROCEDURE procedure_1(id_1 IN INT, name_1 IN VARCHAR2, percent REAL) IS
        x medicines.price%TYPE;
        null_ex EXCEPTION;
BEGIN
    SELECT price INTO x FROM medicines
    WHERE id=id_1 AND name=name_1;
        IF x IS NULL THEN
           RAISE null_ex;
        ELSE UPDATE medicines SET price = price*percent
             WHERE id=id_1 AND name=name_1;
        END IF;
EXCEPTION   
        WHEN NO_DATA_FOUND THEN
             DBMS_OUTPUT.enable;
             DBMS_OUTPUT.put_line('no data found');
        WHEN null_ex THEN
             DBMS_OUTPUT.enable;
             DBMS_OUTPUT.put_line('price is null');
END procedure_1;

/*Функция. Вывести цену препарата по заданному ID.*/

CREATE OR REPLACE FUNCTION function_1(id_1 INT) RETURN INT IS
            prc INT;
BEGIN
            SELECT price INTO prc FROM medicines WHERE id=id_1;
            RETURN prc;
END function_1;

/*Пакет*/

CREATE OR REPLACE PACKAGE pack AS
 PROCEDURE procedure_1(id_1 IN INT, name_1 IN VARCHAR2, percent REAL);
 FUNCTION function_1(id_1 INT) RETURN INT;
END pack;

CREATE OR REPLACE PACKAGE BODY pack AS
 PROCEDURE procedure_1(id_1 IN INT, name_1 IN VARCHAR2, percent REAL) IS
        x medicines.price%TYPE;
        null_ex EXCEPTION;
BEGIN
    SELECT price INTO x FROM medicines
    WHERE id=id_1 AND name=name_1;
        IF x IS NULL THEN
           RAISE null_ex;
        ELSE UPDATE medicines SET price = price*percent
             WHERE id=id_1 AND name=name_1;
        END IF;
EXCEPTION   
        WHEN NO_DATA_FOUND THEN
             DBMS_OUTPUT.enable;
             DBMS_OUTPUT.put_line('no data found');
        WHEN null_ex THEN
             DBMS_OUTPUT.enable;
             DBMS_OUTPUT.put_line('price is null');
END procedure_1;
 FUNCTION function_1(id_1 INT) RETURN INT IS prc INT;
BEGIN
            SELECT price INTO prc FROM medicines WHERE id=id_1;
            RETURN prc;
END function_1;
END pack;

/*Скрипты вызовов процедур и фунцкий*/

BEGIN
 procedure_1(8, 'Empatogenu', 10);
END;

DECLARE
 value REAL;
BEGIN
 value := function_1(7);
 DBMS_OUTPUT.enable;
 DBMS_OUTPUT.put_line(value);
END;